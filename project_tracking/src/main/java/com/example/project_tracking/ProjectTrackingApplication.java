package com.example.project_tracking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main класс приложения ProjectTracking
 * @author Victoria Pavlova
 */
@SpringBootApplication
public class ProjectTrackingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectTrackingApplication.class, args);
    }

}