package com.example.project_tracking.service;

import java.util.Optional;
import com.example.project_tracking.model.Projects;
import com.example.project_tracking.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Класс, реализующий сервисвисный слой
 * в данном классе описана бизнес логика операций
 * создание проекта;
 * удаление проекта;
 * обновление проекта;
 * получение проекта по Id;
 * вывод списка проектов из базы данных;
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    /**
     * Метод создания и сохранения проекта в базу данных
     * @param projects переменная модели данных
     * @return projects
     */
    @Override
    public Projects create(Projects projects) {
     return projectRepository.save(projects);
    }

    /**
     * Метод удаления проекта, используя его идентификатор
     * @param id идентификатор проекта
     */
    @Override
    public void removeProject(String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.deleteById(id);
    }

    /**
     * Метод обновления проекта в базе данных
     * @param projects переменная модели данных
     * @return projects
     */
    @Override
    public Projects updateProject(Projects projects) {
        String projectName = projects.getName();
        if (projectName == null || projectName.isEmpty()) return null;
       return projectRepository.save(projects);

    }

    /**
     * Метод получения объекта по Id
     * @param id идентифмкатор проекта
     * @return найденный объект по идентификатору класса Optional
     */
    @Override
    public Optional<Projects> getProjectsById(String id) {
        if (id == null || id.isEmpty()) return Optional.empty();
        return projectRepository.findById(id);
    }

    /**
     * Метод получения списка проектов из базы данных
     * @return список проектов
     */
    @Override
    public Iterable<Projects> listProjects() {
        return projectRepository.findAll();
    }
}
