package com.example.project_tracking.service;

import java.util.Optional;
import com.example.project_tracking.model.Projects;

public interface ProjectService {

    Projects create(Projects projects);

    void removeProject(String id);

    Projects updateProject(Projects projects);

    Optional<Projects> getProjectsById(String id);

    Iterable<Projects> listProjects();
}
