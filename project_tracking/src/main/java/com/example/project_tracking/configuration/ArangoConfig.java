package com.example.project_tracking.configuration;

import com.arangodb.ArangoDB;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.ArangoConfiguration;
import com.arangodb.springframework.core.ArangoOperations;
import com.example.project_tracking.configuration.property.AppConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

/**
 * Класс конфигурации для базы данных
 * @see ArangoConfiguration
 */
@Configuration
@RequiredArgsConstructor
@EnableArangoRepositories(basePackages = {"com.example.project_tracking"})
public class ArangoConfig implements ArangoConfiguration {

    private final AppConfig appConfig;

    /**
     * Метод для подключения к базе данных
     * @return объект ArangoDB.Builder
     */
    @Override
    public ArangoDB.Builder arango() {
        return new ArangoDB.Builder()
                .host(appConfig.getHost(), appConfig.getPort())
                .password(appConfig.getPassword())
                .user(appConfig.getLogin());
    }

    /**
     * Метод для указания имени базы данных
     * @return имя базы данных
     */
    @Override
    public String database() {
        return "project_tracking";
    }

    /**
     * Метод взаимодействия с базой данных
     * реализующий функции драйвера ArangoDB 
     * @return экземпляр ArangoOperations
     * @throws Exception
     */
    @Override
    public ArangoOperations arangoTemplate() throws Exception {
        return ArangoConfiguration.super.arangoTemplate();
    }

}
