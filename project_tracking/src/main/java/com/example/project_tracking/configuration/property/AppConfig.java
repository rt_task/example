package com.example.project_tracking.configuration.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Класс, описывающий теги для application.yml
 */
@Data
@Component
@ConfigurationProperties(prefix = "db")
public class AppConfig {

    /** Хост базы данных */
    private String host;

    /** Порт базы данных */
    private Integer port;

    /** Логин для базы данных */
    private String login;

    /** Пароль для базы данных */
    private String password;

}
