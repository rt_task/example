package com.example.project_tracking.enumerated;

/**
 * Класс enum для поля Status
 * описывающий в каком состоянии находится проект
 */
public enum Status {

    /** Проект еще в статусе 'не начат' */
    NOT_STARTED("Not started"),
    /** Проект находиться в статусе 'в работе' */
    IN_PROGRESS("In progress"),
    /** Проект находиться в статусе 'завершен' */
    COMPLETE("Complete");

    private final String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
