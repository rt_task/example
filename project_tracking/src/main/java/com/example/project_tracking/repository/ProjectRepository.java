package com.example.project_tracking.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.example.project_tracking.model.Projects;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends ArangoRepository<Projects, String>{

}