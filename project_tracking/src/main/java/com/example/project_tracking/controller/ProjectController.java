package com.example.project_tracking.controller;

import com.example.project_tracking.model.Projects;
import com.example.project_tracking.service.ProjectService;
import com.example.project_tracking.service.ProjectServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Класс контроллер
 * реализующий обработку запросов CRUD операций
 * @version 1.0
 */
@Tag(
        name = "pet-project",
        description = "API projects"
)
@RestController
@RequestMapping("/projects")
public class ProjectController {

    private final Logger log = LoggerFactory.getLogger(ProjectController.class);

    private final ProjectService projectService;

    public ProjectController(ProjectServiceImpl projectService) {
        this.projectService = projectService;
    }

    @PostMapping("/add")
    @Operation(summary = "Добавление проекта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful Operation",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Invalid parameters supplied",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Error",
                    content = @Content)})
    public Projects addProject(
            @RequestBody Projects projects) {
        log.info("Project saved. Project: {}", projects);
        return projectService.create(projects);
    }

    @GetMapping("/list")
    @Operation(summary = "Вывод списка проектов")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful Operation",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Invalid parameters supplied",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Error",
                    content = @Content)})
    public Iterable<Projects> list() {
        log.info("List projects");
        return projectService.listProjects();
    }
    
    @PutMapping("/update")
    @Operation(summary = "Обновление проекта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful Operation",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Internal Error",
                    content = @Content)})
    public Projects update(@RequestBody Projects projects) {
        log.info("Update the project of name {} projects {}", projects.getName(), projects);
        return projectService.updateProject(projects);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Удаление проекта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful Operation",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Internal Error",
                    content = @Content)})
    public void deleteByName(@PathVariable String id) {
        log.info("Delete the project of id: {}", id);
        projectService.removeProject(id);
    }

    @GetMapping("/find/{id}")
    @Operation(summary = "Поиск проекта по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful Operation",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Invalid parameters supplied",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Error",
                    content = @Content)})
    public Optional<Projects> findById(@PathVariable String id) {
        log.info("Load the article of id: {}", id);
        return projectService.getProjectsById(id);
    }

}
