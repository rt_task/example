package com.example.project_tracking.model;

import java.util.UUID;
import com.arangodb.springframework.annotation.ArangoId;
import com.arangodb.springframework.annotation.Document;
import com.example.project_tracking.enumerated.Status;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * Класс, описывающий характеристики проектов базы данных
 * @author Victoria Pavlova
 * @version 1.0
 */
@Data
@NoArgsConstructor
@Document("projects")
public class Projects {

    /**
     * Поле базы данных _key
     */
    @Id
    private String key;

    /**
     * Поле базы данных _id
     */
    @ArangoId
    private String arangoId;

    /** Поле идентификатор */
    private String id = UUID.randomUUID().toString();
    /** Поле имени проекта */
    private String name;
    /** Поле для описания проекта */
    private String description;
    /** Поле, описывающее в каком статусе находиться проект */
    private Status status = Status.NOT_STARTED;
    /** Поле, определяющее приоритетность проекта */
    private String priority;
    /** Поле, где указана дата начала проекта */
    private String dateStart;

}
