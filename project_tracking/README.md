# DEVELOPER INFO 

NAME: Victoria Pavlova

E-MAIL: vikkkapaw@mail.ru
_______________________

# Application project_tracking [![Gitlab logo](https://cdn.icon-icons.com/icons2/2699/PNG/128/gitlab_tile_logo_icon_170092.png)](https://gitlab.com/rt_task/example/-/tree/version2.0)

## Branch Git

* version2.0

# OPERATIONS

* update
* add
* list
* findId
* delete

# RUN PROGRAM

``` Docker
docker compose up
```
## SWAGGER

``` 
http://localhost:9090/swagger-ui/index.html#/
```
